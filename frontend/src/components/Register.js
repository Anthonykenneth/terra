import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { registerUser } from "../actions/authentication";
// import classnames from "classnames";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      password_confirm: "",
      profile: {
        firstname: "",
        lastname: ""
      },
      errors: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const user = {
      email: this.state.email,
      password: this.state.password,
      password_confirm: this.state.password_confirm,
      profile: this.state.profile
    };
    this.props.registerUser(user, this.props.history);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  render() {
    //const { errors } = this.state;
    return (
      <div className="container" style={{ marginTop: "50px", width: "400px" }}>
        <h2 style={{ marginBottom: "40px", marginLeft: "100px" }}>
          Registration
        </h2>
        <br />
        <div className="text-danger">{this.state.errors}</div>
        <br />
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <input
              type="text"
              placeholder="firstname"
              className="form-control form-control-md"
              name="firstname"
              onChange={this.handleInputChange}
              value={this.state.profile.lasttname}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              placeholder="Lastname"
              className="form-control form-control-md"
              name="lastname"
              onChange={this.handleInputChange}
              value={this.state.profile.lasttname}
            />
          </div>
          <div className="form-group">
            <input
              type="email"
              placeholder="Email"
              className="form-control form-control-md"
              name="email"
              onChange={this.handleInputChange}
              value={this.state.email}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Password"
              className="form-control form-control-md"
              name="password"
              onChange={this.handleInputChange}
              value={this.state.password}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              placeholder="Confirm Password"
              className="form-control form-control-md"
              name="password_confirm"
              onChange={this.handleInputChange}
              value={this.state.password_confirm}
            />
          </div>
          <div className="form-group" style={{ marginLeft: "120px" }}>
            <button type="submit" className="btn btn-primary">
              Register User
            </button>
          </div>
        </form>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { registerUser }
)(withRouter(Register));
